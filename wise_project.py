import sqlite3
import tkinter as tk
from tkinter import messagebox
import re

fonts = ('Courier New', 15, 'bold')

class Login:
    def __init__(self, root,cursor):
        self.root = root
        self.child_database = []  # Store child details
        self.cursor = cursor
        

        image_path = 'bg.png'
        self.img = tk.PhotoImage(file=image_path)
        self.image_label = tk.Label(root, image=self.img, bg='white')
        self.image_label.place(relx=0.5, rely=0.5, anchor='center')

        self.login_frame = tk.Frame(self.root, width=200, height=100, bg='hotpink')
        self.login_frame.pack(pady=100, side='right', anchor='e', padx=180)
        self.login_label = tk.Label(self.login_frame, text="LOGIN", font=('Helvetica', 40, 'bold'), fg='white', bg='hotpink')
        self.login_label.grid(row=0, column=1, columnspan=2)

        self.user_name = tk.Label(self.login_frame, text='NAME', font=('Helvetica', 20, 'bold'), bg='hotpink', fg='indigo', width=12)
        self.user_name.grid(row=3, column=1, pady=10)
        self.user_name_entry = tk.Entry(self.login_frame, width=15, font=fonts, bg='white')
        self.user_name_entry.grid(row=3, column=2, pady=10)

        self.user_pass = tk.Label(self.login_frame, text="PASSWORD", font=('Helvetica', 20, 'bold'), bg='hotpink', fg='indigo', width=12)
        self.user_pass.grid(row=5, column=1, pady=10)
        self.user_pass_entry = tk.Entry(self.login_frame, width=15, font=fonts, bg='white', show="*")
        self.user_pass_entry.grid(row=5, column=2, pady=10)

        self.admin_login_btn = tk.Button(self.login_frame, text='Orphanage', bg='white', fg='steel blue', font=fonts,
                                         command=self.open_orphanage_window, cursor='hand2', activebackground='blue')
        self.admin_login_btn.grid(row=7, column=0, columnspan=2)

        self.parent_login_btn = tk.Button(self.login_frame, text='Parent', bg='white', fg='steel blue', font=fonts,
                                          command=self.open_parent_window, cursor='hand2', activebackground='blue')
        self.parent_login_btn.grid(row=7, column=2, columnspan=2)

        self.additional_button = tk.Button(self.login_frame, text='Registration', bg='white', fg='blue', font=fonts,
                                           command=self.registration_window, cursor='hand2', activebackground='lightblue')
        self.additional_button.grid(row=9, column=0, columnspan=4, pady=20)

        self.reg_label = tk.Label(self.login_frame, text="Don't have an account? ", bg='hotpink', font=('Helvetica', 14, 'bold'), fg='white')
        self.reg_label.grid(row=8, column=0, columnspan=4, padx=5)

    

    def open_orphanage_window(self):
        admin_window = tk.Toplevel()
        admin_window.title("Admin Menu")
        admin_window.geometry("1920x1080")

        image_path = "orphandetail.png"  # Replace with the actual path to your image
        img = tk.PhotoImage(file=image_path)
        img_label = tk.Label(admin_window, image=img)
        img_label.image = img  # Keep a reference to prevent garbage collection
        img_label.place(x=0, y=0, relwidth=1, relheight=1)

        label = tk.Label(admin_window)
        label.pack()
        admin_window.protocol("WM_DELETE_WINDOW", self.root.quit)  # Close the app when the admin window is closed

        child_frame = tk.Frame(admin_window,bg="#92FBFF")
        child_frame.pack(side='right', anchor='e', padx=200, pady=200)



        name_label = tk.Label(child_frame, text="Name",font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        name_label.grid(row=1, column=6)
        self.name_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.name_entry.grid(row=1, column=7,pady=10)

        age_label = tk.Label(child_frame, text="Age", font=('Helvetica', 20,'bold'), fg='hot pink', bg="#92FBFF")
        age_label.grid(row=2, column=6)
        self.age_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.age_entry.grid(row=2, column=7)

        gender_label = tk.Label(child_frame, text="Gender", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        gender_label.grid(row=3, column=6)
        self.gender_var = tk.StringVar()
        self.gender_var.set("                Select                        ") 
        gender_options = ["                 Male                      ", "                 Female                  "]
        gender_menu = tk.OptionMenu(child_frame, self.gender_var, *gender_options)
        gender_menu.grid(row=3, column=7)

        phone_label = tk.Label(child_frame, text="Phone no", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        phone_label.grid(row=4, column=6)
        self.phone_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.phone_entry.grid(row=4, column=7)

        health_label = tk.Label(child_frame, text="Disabilities", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        health_label.grid(row=5, column=6)
        self.health_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.health_entry.grid(row=5, column=7)

        orphan_home_name_label = tk.Label(child_frame, text="Orphan Home Name", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        orphan_home_name_label.grid(row=6, column=6)
        self.orphan_home_name_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.orphan_home_name_entry.grid(row=6, column=7)

        orphan_home_address_label = tk.Label(child_frame, text="Orphan Home Address", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        orphan_home_address_label.grid(row=7, column=6)
        self.orphan_home_address_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.orphan_home_address_entry.grid(row=7, column=7)

        add_child_button = tk.Button(child_frame, text="Add Child", command=self.add_child, bg='green', fg='white', activebackground='blue')
        add_child_button.grid(row=8, column=5, columnspan=2, pady=10)



    def add_child(self):
      name = self.name_entry.get()
      age = self.age_entry.get()
      gender = self.gender_var.get()
      phone = self.phone_entry.get()
      health_update = self.health_entry.get()
      orphan_home_name = self.orphan_home_name_entry.get()
      orphan_home_address = self.orphan_home_address_entry.get()

    # Insert data into the orphans table
      self.cursor.execute('''
        INSERT INTO orphans (name, age, gender, phone, disabilities, orphan_home_name, orphan_home_address)
        VALUES (?, ?, ?, ?, ?, ?, ?)
       ''', (name, age, gender, phone, health_update, orphan_home_name, orphan_home_address))

      self.conn.commit()

      self.clear_entry_fields()

      messagebox.showinfo("Child Added", "Child details added successfully!")



    def open_parent_window(self):
        parent_window = tk.Toplevel()
        parent_window.title("Parent Window")
        parent_window.geometry("1920x1080")



        image_path = "child_details.png"  # Replace with the actual path to your image
        img = tk.PhotoImage(file=image_path)
        img_label = tk.Label(parent_window, image=img)
        img_label.image = img  # Keep a reference to prevent garbage collection
        img_label.place(x=0, y=0, relwidth=1, relheight=1)

        label = tk.Label(parent_window)
        label.pack()
        parent_window.protocol("WM_DELETE_WINDOW", self.root.quit)  # Close the app when the Parent window is closed

        parent_window = tk.Frame(parent_window,bg="#92FBFF")
        parent_window.pack(side='right', anchor='e', padx=600, pady=100)

        tk.Label(parent_window, text="Gender:", font=('Helvetica', 14), fg='black',bg="#92FBFF").grid(row=0, column=0, pady=10)
        self.gender_var = tk.StringVar()
        self.gender_var.set("Boy")  # Default value
        gender_options = ["Boy", "Girl"]
        gender_menu = tk.OptionMenu(parent_window, self.gender_var, *gender_options)
        gender_menu.grid(row=0, column=1, pady=10)

        tk.Label(parent_window, text="Age:", font=('Helvetica', 14), fg='black',bg="#92FBFF").grid(row=1, column=0, pady=10)
        self.age_var = tk.IntVar()
        age_spinbox = tk.Spinbox(parent_window, from_=1, to=18, textvariable=self.age_var, font=fonts, bg='white')
        age_spinbox.grid(row=1, column=1, pady=10)

        '''tk.Label(parent_window, text="State:", font=('Helvetica', 14), fg='black',bg="#92FBFF").grid(row=2, column=0, pady=10)
        self.state_var = tk.StringVar()
        state_options = ["Andhra Pradesh", "Kerala"]
        state_menu = tk.OptionMenu(parent_window, self.state_var, *state_options)
        state_menu.grid(row=2, column=1, pady=10)'''


        tk.Button(parent_window, text='Child Details', bg='hot pink', fg='black', font=fonts
                  , activebackground='blue').grid(row=4, columnspan=2, pady=20)

    '''def open_child_details_window(self):
        child_details_window = tk.Toplevel()
        child_details_window.title("Child Details")
        child_details_window.geometry("800x600")
        # You can add child details display or any other relevant content here'''

    def view_child_details(self, parent_window):
        child_frame = tk.Frame(parent_window)
        child_frame.pack(pady=20)

        tk.Label(child_frame, text="Child Details", font=("bold", 17), fg='orange').grid(row=0, columnspan=2)

        headers = ["Name", "Age", "Gender", "Phone no", "Disabilities", "Orphan Home Name", "Orphan Home Address"]
        for col, header in enumerate(headers):
            tk.Label(child_frame, text=header, font=("bold", 12), fg='blue').grid(row=1, column=col, padx=5, pady=5)

        for row, child in enumerate(self.child_database):
            for col, key in enumerate(headers):
                value = child.get(key, "")
                tk.Label(child_frame, text=value, font=("normal", 10), fg='black').grid(row=row + 2, column=col, padx=5, pady=5)

    
    def registration_window(self):
      image_path = 'bg.png'
      self.img = tk.PhotoImage(file=image_path)
      self.image_label = tk.Label(root, image=self.img, bg='white')
      self.image_label.place(relx=0.5, rely=0.5, anchor='center')

      self.login_frame = tk.Frame(self.root, width=200, height=100, bg='hotpink')
      self.login_frame.place(relx=0.85, rely=0.5, anchor='e')

      self.login_label = tk.Label(self.login_frame, text="Don't have an account??\nRegister now.. ", font=('Helvetica', 20, 'bold'), fg='white', bg='hotpink')
      self.login_label.grid(row=0, column=0, columnspan=2)

      self.admin_login_btn = tk.Button(self.login_frame, text='Orphanage', bg='white', fg='steel blue', font=fonts,
                                     command=self.open_Oregistration_window, activebackground='blue')
      self.admin_login_btn.grid(row=3, column=0, columnspan=2, pady=10)

      self.parent_login_btn = tk.Button(self.login_frame, text='Parent', bg='white', fg='steel blue', font=fonts,
                                      command=self.open_Pregistration_window, activebackground='blue')
      self.parent_login_btn.grid(row=6, column=0, columnspan=2, pady=10)


        
    def open_Oregistration_window(self):
        o_registration_window = tk.Toplevel(self.root)
        o_registration_window.title("ORegistration")
        o_registration_window.geometry("1920x1080")

        o_registration_frame = tk.Frame(o_registration_window, width=100, height=100)
        o_registration_frame.pack(expand=True, fill='both')

        o_reg_image_path = 'reg.png'
        o_reg_img = tk.PhotoImage(file=o_reg_image_path)
        o_reg_image_label = tk.Label(o_registration_frame, image=o_reg_img, bg='white', width=10, height=10)
        o_reg_image_label.image = o_reg_img
        o_reg_image_label.pack(expand=True, fill='both')

        o_reg_label = tk.Label(o_registration_frame, text="Registration Details", font=('Helvetica', 30, 'bold'), fg='hot pink', bg='white')
        o_reg_label.place(relx=0.4, rely=0.05, anchor='n')  # Set coordinates to overlap the image

        o_details_labels = ["Ophanage name:", "Address:", "Mobile Number:", "Email:", "Username:", "Password:"]
        o_details_entries = []

        for i, o_label_text in enumerate(o_details_labels):
            o_label = tk.Label(o_registration_frame, text=o_label_text, font=('Helvetica', 14), fg='black' , bg='white')
            o_label.place(relx=0.25, rely=0.15 + i * 0.1, anchor='w')  
            o_entry = tk.Entry(o_registration_frame, font=('Helvetica', 12), bg='white')
            o_entry.place(relx=0.55, rely=0.15 + i * 0.1, anchor='e')  
            o_details_entries.append(o_entry)

        o_register_button = tk.Button(o_registration_frame, text='Finish Registration', bg='hotpink', fg='white', font=('Helvetica', 14),
                                    command=lambda: self.o_validate_and_register(o_registration_window, o_details_entries), activebackground='white')

        o_register_button.place(relx=0.4, rely=0.78, anchor='s') 

    def o_validate_and_register(self, registration_window, o_details_entries):
        def valid_name(orphanage_name):
            return orphanage_name.isalpha()

        def valid_mobile_number(o_mobile_number):
            return o_mobile_number.isdigit() and len(o_mobile_number) == 10

        def valid_email(o_email):
            return re.match(r"[^@]+@[^@]+\.[^@]+", o_email)

        def valid_username(o_username):
            return o_username.isalnum()

        orphanage_name, address, o_mobile_number, o_email, o_username, o_password = [o_entry.get() for o_entry in o_details_entries]

        if not valid_name(orphanage_name):
            self.show_error_message("Invalid orphange Name")
        elif not valid_name(address):
            self.show_error_message("Invalid address")
        elif not valid_mobile_number(o_mobile_number):
            self.show_error_message("Invalid Mobile Number")
        elif not valid_email(o_email):
            self.show_error_message("Invalid Email")
        elif not valid_username(o_username):
            self.show_error_message("Invalid Username")
        else:
            self.register_admin(registration_window, o_details_entries)
            self.registration_successful(registration_window)


    def open_Pregistration_window(self):
        registration_window = tk.Toplevel(self.root)
        registration_window.title("PRegistration")
        registration_window.geometry("1920x1080")

        registration_frame = tk.Frame(registration_window, width=100, height=100)
        registration_frame.pack(expand=True, fill='both')

        reg_image_path = 'reg.png'
        reg_img = tk.PhotoImage(file=reg_image_path)
        reg_image_label = tk.Label(registration_frame, image=reg_img, bg='white', width=10, height=10)
        reg_image_label.image = reg_img
        reg_image_label.pack(expand=True, fill='both')

        reg_label = tk.Label(registration_frame, text="Registration Details", font=('Helvetica', 30, 'bold'), fg='hot pink', bg='white')
        reg_label.place(relx=0.4, rely=0.05, anchor='n')  # Set coordinates to overlap the image

        details_labels = ["First_Name:", "Last_Name:", "Mobile_Number:", "Email:", "Username:", "Password:"]
        details_entries = []

        for i, label_text in enumerate(details_labels):
            label = tk.Label(registration_frame, text=label_text, font=('Helvetica', 14), fg='black' , bg='white')
            label.place(relx=0.25, rely=0.15 + i * 0.1, anchor='w')  # Adjust coordinates as needed
            entry = tk.Entry(registration_frame, font=('Helvetica', 12), bg='white')
            entry.place(relx=0.55, rely=0.15 + i * 0.1, anchor='e')  # Adjust coordinates as needed
            details_entries.append(entry)

        register_button = tk.Button(registration_frame, text='Finish Registration', bg='hotpink', fg='white', font=('Helvetica', 14),
                                    command=lambda: self.validate_and_register(registration_window, details_entries), activebackground='white')
        register_button.place(relx=0.4, rely=0.78, anchor='s')  # Set coordinates to overlap the image


    def validate_and_register(self, registration_window, details_entries):
        def is_valid_name(name):
            return name.isalpha()

        def is_valid_mobile_number(number):
            return number.isdigit() and len(number) == 10

        def is_valid_email(email):
            return re.match(r"[^@]+@[^@]+\.[^@]+", email)

        def is_valid_username(username):
            return username.isalnum()

        first_name, last_name, mobile_number, email, username, password = [entry.get() for entry in details_entries]

        if not is_valid_name(first_name):
            self.show_error_message("Invalid First Name")
        elif not is_valid_name(last_name):
            self.show_error_message("Invalid Last Name")
        elif not is_valid_mobile_number(mobile_number):
            self.show_error_message("Invalid Mobile Number")
        elif not is_valid_email(email):
            self.show_error_message("Invalid Email")
        elif not is_valid_username(username):
            self.show_error_message("Invalid Username")
        else:
            self.register_parent(registration_window, details_entries)
            self.registration_successful(registration_window)

    def show_error_message(self, message):
        tk.messagebox.showerror("Error", message)

    def registration_successful(self, registration_window):
        
        messagebox.showinfo("Registration Successful", "Registration completed successfully!")
        registration_window.destroy()

    def clear_entry_fields(self):
        self.name_entry.delete(0, tk.END)
        self.age_entry.delete(0, tk.END)
        self.phone_entry.delete(0, tk.END)
        self.health_entry.delete(0, tk.END)
        self.orphan_home_name_entry.delete(0, tk.END)
        self.orphan_home_address_entry.delete(0, tk.END)

    def add_child(self):
        name = self.name_entry.get()
        age = self.age_entry.get()
        gender = self.gender_var.get()
        phone = self.phone_entry.get()
        health_update = self.health_entry.get()
        orphan_home_name = self.orphan_home_name_entry.get()
        orphan_home_address = self.orphan_home_address_entry.get()

        # Insert data into the orphans table
        cursor.execute('''
            INSERT INTO orphans (name, age, gender, phone, disabilities, orphan_home_name, orphan_home_address)
            VALUES (?, ?, ?, ?, ?, ?, ?)
        ''', (name, age, gender, phone, health_update, orphan_home_name, orphan_home_address))

        conn.commit()

        self.clear_entry_fields()

        messagebox.showinfo("Child Added", "Child details added successfully!")
    
    def register_parent(self, Pregistration_window, details_entries):
        first_name, last_name, mobile_number, email, username, password = [entry.get() for entry in details_entries]
        cursor.execute('''INSERT INTO parents (first_name, last_name, mobile_number, email, username, password) 
                       VALUES (?, ?, ?, ?, ?, ?)''', (first_name, last_name, mobile_number, email, username, password))
        conn.commit()
        self.registration_successful(Pregistration_window)

    def register_admin(self, Oregistration_window, o_details_entries):
       orphanage_name, address, o_mobile_number, o_email, o_username, o_password = [o_entry.get() for o_entry in o_details_entries]
       cursor.execute('''
        INSERT INTO orphanage (orphanage_name, address, o_mobile_number, o_email, o_username, o_password)
        VALUES (?, ?, ?, ?, ?, ?)
        ''', (orphanage_name, address, o_mobile_number, o_email, o_username, o_password))
       conn.commit()
       self.registration_successful(Oregistration_window)


   

if __name__ == "__main__":
    # Connect to SQLite database
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    if(conn):
        print("successfully connected")
    # Create tables for orphan and parent registration
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS orphans (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            age INTEGER NOT NULL,
            gender TEXT,
            phone TEXT,
            disabilities TEXT,
            orphan_home_name TEXT,
            orphan_home_address TEXT
        )
    ''')
    conn.commit()

    cursor.execute('''
        CREATE TABLE IF NOT EXISTS parents (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT NOT NULL,
            last_name TEXT NOT NULL,
            mobile_number TEXT,
            email TEXT,
            username TEXT NOT NULL,
            password TEXT NOT NULL
        )
    ''')
    

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS orphanage (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        orphanage_name TEXT NOT NULL,
        address TEXT NOT NULL,
        o_mobile_number TEXT,
        o_email TEXT,
        o_username TEXT NOT NULL,
        o_password TEXT NOT NULL
       )
   ''')
    conn.commit()




    conn.commit()

    root = tk.Tk()
    root.title("Heart Bound")
    root.configure(bg="white")
    root.geometry('1920x1080')
    root.resizable(True, True)

    app = Login(root,cursor)
    root.mainloop()

    # Close the database connection
    conn.close()